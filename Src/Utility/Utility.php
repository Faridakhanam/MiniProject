<?php
    namespace Src\Utility;
    
    class Utility{
      static public function d($param=false){
        echo "<pre>";
        var_dump($param);
        echo "</pre>";
    }
    
    static public function dd($param=false){
        self::d($param);
        die();
    }
    
    static public function redirect($url="/Registration_Form/Views/index."){
        header("Location:".$url);
            }
}
?>